extends "res://actors/equip_handler.gd"

func _on_EquipArea_body_entered(body):
	._on_EquipArea_body_entered(body)
	_display_equip_hint()


func _on_EquipArea_body_exited(body):
	._on_EquipArea_body_exited(body)
	_display_equip_hint()


func _display_equip_hint():
	var equip_hint = "Equip %s (x)" % bodies_in_area[0].display_name if bodies_in_area.size() > 0 else ""

	NodeRegistry.hud.equip_hint = equip_hint
