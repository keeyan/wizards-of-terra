extends KinematicBody
class_name Eirenite

const Dialog = preload("res://ui/dialog/dialog.tscn")

export(String) var display_name
export(Script) var dialog_tree

func trigger_interaction(interactor):
	# TODO: Add generic greetings if no dialog
	if not dialog_tree: return
	
	var dialog = Dialog.instance()
	dialog.dialog_tree = dialog_tree.new()
	NodeRegistry.ui.add_child(dialog)
	dialog.begin_dialog()
