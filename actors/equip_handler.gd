extends Area

export(NodePath) var equip_area_path

var bodies_in_area = []
var equiped_item
var equipable_item setget, _get_equipable_item

func equip_item():
	var equipable_item = _get_equipable_item()

	if equipable_item == null: return
	
	_drop_equiped_item()
	
	bodies_in_area.erase(equipable_item)
	equiped_item = equipable_item
	equiped_item.get_parent().remove_child(equiped_item)
	_equip_area().add_child(equiped_item)
	equiped_item.transform = Transform()

func _drop_equiped_item():
	if equiped_item == null: return
	var dropped_item_global_transform = equiped_item.global_transform
	dropped_item_global_transform.origin.y -= 0.5
	
	_equip_area().remove_child(equiped_item)
	NodeRegistry.world.add_child(equiped_item)

	# Resets the forces
	equiped_item.mode = RigidBody.MODE_STATIC
	equiped_item.mode = RigidBody.MODE_RIGID

	equiped_item.global_transform = dropped_item_global_transform
	# Wakes up the body in case it was sleeping
	equiped_item.apply_central_impulse(Vector3(0.1, 0.1, 0.1))
	equiped_item = null

func _equip_area():
	return get_node(equip_area_path)

func _get_equipable_item():
	if bodies_in_area.size() == 0: return null
	
	return bodies_in_area[0]

func _on_EquipArea_body_entered(body):
	if !body.is_in_group("equipable") || body == equiped_item: return

	bodies_in_area.append(body)



func _on_EquipArea_body_exited(body):
	if not body.is_in_group("equipable"): return

	bodies_in_area.erase(body)
