extends "res://actors/eirenite.gd"

const HORIZONTAL_SENSITIVITY = 0.1
const VERTICAL_SENSITIVITY = 0.05
const SPEED = 10
const ACCEL_DEFAULT = 10
const ACCEL_AIR = 1
const GRAVITY = 9.7
const JUMP = 5
const MIN_VERTICAL_LOOK_DEG = -40
const MAX_VERTICAL_LOOK_DEG = 50

var velocity = Vector3()
var gravity_vec = Vector3()
var snap
var acceleration

onready var pivot = $Pivot
onready var interact_ray_cast = $Pivot/Camera/InteractRayCast
onready var point_at_ray_cast = $Pivot/Camera/PointAtRayCast
onready var equip_handler = $PlayerEquipHandler
onready var equip_area = $EquipArea
onready var hud = NodeRegistry.hud

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	interact_ray_cast.add_exception(self)
	point_at_ray_cast.add_exception(self)


func _process(delta):
	_display_interact_hint()
	_point_weapon_at_point_at_point(delta)


func _display_interact_hint():
	if !ControlsHandler.controller_is(ControlsHandler.PLAYER): return

	if !interact_ray_cast.is_colliding():
		hud.interact_hint = ''
		return
	
	var collider = interact_ray_cast.get_collider()
	
	if collider.is_in_group("interactable"):
		hud.interact_hint = "Interact with %s (e)" % collider.display_name
	else:
		hud.interact_hint = ''


func _input(event):
	if !ControlsHandler.controller_is(ControlsHandler.PLAYER): return

	if event is InputEventMouseMotion:
		rotate_y(deg2rad(-event.relative.x * HORIZONTAL_SENSITIVITY))
		pivot.rotate_x(deg2rad(-event.relative.y * VERTICAL_SENSITIVITY))
		pivot.rotation.x = clamp(
			pivot.rotation.x,
			deg2rad(MIN_VERTICAL_LOOK_DEG),
			deg2rad(MAX_VERTICAL_LOOK_DEG)
		)
	_check_and_trigger_interaction(event)
	_check_and_trigger_equip(event)

func _point_weapon_at_point_at_point(delta):
	var equiped_item = equip_handler.equiped_item

	if equiped_item == null: return

	var pointing_at = _point_at_ray_cast_pointing_at_point()
	if equiped_item.global_transform.origin.distance_to(pointing_at) < 2.5: return

	var target_transform = equiped_item.global_transform.looking_at(pointing_at, Vector3.UP)
	equiped_item.global_transform = target_transform

func _point_at_ray_cast_pointing_at_point():
	if point_at_ray_cast.is_colliding():
		return point_at_ray_cast.get_collision_point()
	else:
		return point_at_ray_cast.global_transform.translated(point_at_ray_cast.cast_to).origin

func _check_and_trigger_interaction(event):
	if not event.is_action_pressed("interact"): return
	
	var collider = interact_ray_cast.get_collider()

	if not collider.is_in_group("interactable"): return

	collider.trigger_interaction(self)

func _check_and_trigger_equip(event):
	if not event.is_action_pressed("equip"): return

	equip_handler.equip_item()

func _physics_process(delta):
	_handle_movement(delta)


func _handle_movement(delta):
	var direction = Vector3()

	if ControlsHandler.controller_is(ControlsHandler.PLAYER):
		direction = _movement_direction()
	_process_vertical_acceleration(delta)

	velocity = velocity.linear_interpolate(direction * SPEED, acceleration * delta)
	var movement = velocity + gravity_vec
	move_and_slide_with_snap(movement, snap, Vector3.UP)


func _movement_direction():
	var direction = Vector3()
	if Input.is_action_pressed("move_forward"):
		direction -= transform.basis.z
	if Input.is_action_pressed("move_backward"):
		direction += transform.basis.z
	if Input.is_action_pressed("move_left"):
		direction -= transform.basis.x
	if Input.is_action_pressed("move_right"):
		direction += transform.basis.x
	return direction.normalized()


func _process_vertical_acceleration(delta):
	if is_on_floor():
		snap = -get_floor_normal()
		acceleration = ACCEL_DEFAULT
		gravity_vec = Vector3.ZERO
	else:
		snap = Vector3.DOWN
		acceleration = ACCEL_AIR
		gravity_vec += Vector3.DOWN * GRAVITY * delta
		
	if _should_process_jump():
		snap = Vector3.ZERO
		gravity_vec = Vector3.UP * JUMP

func _should_process_jump():
	return (
		ControlsHandler.controller_is(ControlsHandler.PLAYER) and
		Input.is_action_just_pressed("jump") and
		is_on_floor()
	)
