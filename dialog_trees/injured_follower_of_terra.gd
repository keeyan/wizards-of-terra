extends "dialog_tree.gd"

enum STATES {
	START
	ACCEPTED_TO_HELP
	REJECTED_TO_HELP
}

enum OPTIONS {
	ACCEPT_TO_HELP
	REJECT_TO_HELP
}

# TODO: Add more dialog explaining what is going on
const states_lines = {
	STATES.START: [
		"Hey you! The followers need your help!",
		"I fell whilst hunting a traitor, she's gone to the village and we need your help to get her.",
		"Will you help?"
	],
	STATES.ACCEPTED_TO_HELP: ["Good! Get that staff I dropped and kill her"],
	STATES.REJECTED_TO_HELP: ["Consider your self an enemy of the Wizards of Terra!"]
}

var options = {
	OPTIONS.ACCEPT_TO_HELP: DialogOption.new(OPTIONS.ACCEPT_TO_HELP, "I will help you.", true),
	OPTIONS.REJECT_TO_HELP: DialogOption.new(OPTIONS.REJECT_TO_HELP, "I will not help you.", true)
}

var states_options = {
	STATES.START: [
		options[OPTIONS.ACCEPT_TO_HELP],
		options[OPTIONS.REJECT_TO_HELP]
	]
}

var options_states = {
	OPTIONS.ACCEPT_TO_HELP: STATES.ACCEPTED_TO_HELP,
	OPTIONS.REJECT_TO_HELP: STATES.REJECTED_TO_HELP
}

var current_state = 0

func lines():
	return states_lines[current_state]

func options():
	if !states_options.has(current_state): return []

	return states_options[current_state]

func select_option(option):
	current_state = options_states[option.id]
