extends Node
class_name DialogOption

var id
var text
var leave_conversation

func _init(id, text, leave_conversation = false):
	self.id = id
	self.text = text
	self.leave_conversation = leave_conversation
