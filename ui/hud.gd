extends Control

var interact_hint = null setget _set_interact_hint
var equip_hint = null setget _set_equip_hint

func _set_interact_hint(new_interact_hint):
	interact_hint = new_interact_hint
	$InteractHint.text = interact_hint


func _set_equip_hint(new_equip_hint):
	equip_hint = new_equip_hint
	$EquipHint.text = equip_hint
