extends Control

var dialog_tree
var selected_dialog_option

const Option = preload("res://ui/dialog/option.tscn")

onready var text_area = $Panel/VSplitContainer/TextMargin/Text
onready var options_area = $Panel/VSplitContainer/OptionsMargin/Options

var current_dialog_line = -1

func begin_dialog():
	ControlsHandler.push_controller(ControlsHandler.DIALOG)
	_display_next_dialog_line()


func _input(input):
	if !ControlsHandler.controller_is(ControlsHandler.DIALOG): return

	if input.is_action_pressed("next_dialog_option"):
		_display_next_dialog_line()

	if input.is_action_pressed("dialog_option_1"):
		_select_option(1)
	if input.is_action_pressed("dialog_option_2"):
		_select_option(2)
	if input.is_action_pressed("dialog_option_3"):
		_select_option(3)
	if input.is_action_pressed("dialog_option_4"):
		_select_option(4)
	if input.is_action_pressed("dialog_option_5"):
		_select_option(5)

func _display_next_dialog_line():
	var dialog_tree_lines_size = dialog_tree.lines().size()
	if current_dialog_line + 1 == dialog_tree_lines_size:
		if selected_dialog_option && selected_dialog_option.leave_conversation:
			NodeRegistry.ui.remove_child(self)
			ControlsHandler.release_controller(ControlsHandler.DIALOG)
			return
		else:
			return

	current_dialog_line += 1
	text_area.text = dialog_tree.lines()[current_dialog_line]

	if current_dialog_line + 1 == dialog_tree_lines_size:
		_display_dialog_options()

func _display_dialog_options():
	for child in options_area.get_children():
		options_area.remove_child(child)

	for option in dialog_tree.options():
		var option_ui = Option.instance()
		option_ui.text = option.text
		options_area.add_child(option_ui)

func _select_option(option_number):
	if !dialog_tree.options.has(option_number - 1): return

	selected_dialog_option = dialog_tree.options[option_number - 1]
	dialog_tree.select_option(selected_dialog_option)
	current_dialog_line = -1
	_display_next_dialog_line()
