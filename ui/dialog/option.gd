extends Control

var text setget _set_text

func _set_text(new_text):
	text = new_text
	$RichTextLabel.text = "> " + text
