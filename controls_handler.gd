extends Node

var controller_stack = [PLAYER]

enum {
	PLAYER
	DIALOG
}

func controller_is(controller):
	if controller_stack.empty() && controller != null: return false

	return controller_stack[0] == controller

func controller():
	if controller_stack.empty(): return

	return controller_stack[0]

func push_controller(controller):
	controller_stack.push_front(controller)

func release_controller(controller):
	call_deferred("_release_controller_deferred", controller)

func _release_controller_deferred(controller):
	controller_stack.erase(controller)
